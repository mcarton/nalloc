use std::process::Command;
use std::{env, os::unix::process::ExitStatusExt};

use nalloc::NAlloc;

#[global_allocator]
static ALLOCATOR: NAlloc<std::alloc::System> =
    NAlloc::<std::alloc::System>::new(std::alloc::System);

fn test_abort(test_name: &str, f: impl FnOnce()) {
    if env::var_os("NALLOC_TEST").is_none() {
        let status = Command::new(env::current_exe().expect("Could not get current executable"))
            .arg("--exact")
            .arg(test_name)
            .env("NALLOC_TEST", "true")
            .output()
            .expect("Recursive call failed");

        assert_eq!(status.status.signal(), Some(6));

        return;
    }

    f();
}

#[test]
fn off() {
    let _ = Vec::<u128>::with_capacity(10);
}

#[test]
fn on() {
    test_abort("on", || {
        let _lock = ALLOCATOR.deny();
        let _ = Vec::<u128>::with_capacity(10);
    });
}

#[test]
fn on_and_off() {
    {
        let _lock = ALLOCATOR.deny();
    }

    let _ = Vec::<u128>::with_capacity(10);
}

#[test]
fn multiple_and_off() {
    {
        let _lock = ALLOCATOR.deny();
        let _lock = ALLOCATOR.deny();
        let _lock = ALLOCATOR.deny();
        let _lock = ALLOCATOR.deny();
        let _lock = ALLOCATOR.deny();
    }

    let _ = Vec::<u128>::with_capacity(10);
}

#[test]
fn multiple_and_partial_off_before() {
    test_abort("multiple_and_partial_off_before", || {
        let _lock = ALLOCATOR.deny();

        {
            let _lock = ALLOCATOR.deny();
            let _lock = ALLOCATOR.deny();
            let _lock = ALLOCATOR.deny();
            let _lock = ALLOCATOR.deny();
        }

        let _ = Vec::<u128>::with_capacity(10);
    });
}

#[test]
fn multiple_and_partial_off_after() {
    test_abort("multiple_and_partial_off_after", || {
        {
            let _lock = ALLOCATOR.deny();
            let _lock = ALLOCATOR.deny();
            let _lock = ALLOCATOR.deny();
            let _lock = ALLOCATOR.deny();
        }

        let _lock = ALLOCATOR.deny();
        let _ = Vec::<u128>::with_capacity(10);
    });
}
